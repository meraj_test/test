<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    } 

    
    public function index()
    {
        //
        $BookData=Book::all();
 

        return $BookData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $image = $request->file('cover_image');
            $extension = strtolower($image->getClientOriginalExtension());
            $uniqueName = md5($image . time());
            $uniquename=$uniqueName . '.' . $extension;
            $var=  $image->move(base_path() . '/assets/img', $uniquename);


         $Book=new Book;
 
         $Book->book_name=$request->book_name;
         $Book->author_name=$request->author_name;
         $Book->cover_image=$uniquename;
         
         if($Book->save()){
 
             $response = [
                 'code_status' => '200',
                 'message' => 'Book Detail Saved successfully'
             ];
         
              return response($response, 201);
 
         }else{
 
             $response = [
                 
                 'message' => 'Not Saved something wrong'
             ];
         
              
 
         }
            
        
           // $image->move(public_path($path), $uniqueName . '.' . $extension);
         
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
        $Booksingle=Book::find($book);
         return $Booksingle;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //

    $Book=Book::find($book->id);

     if($request->file('cover_image')){

     $image = $request->file('cover_image');
     $extension = strtolower($image->getClientOriginalExtension());
     $uniqueName = md5($image . time());
     $uniquename=$uniqueName . '.' . $extension;
     $var=  $image->move(base_path() . '/assets/img', $uniquename);
     $Book->cover_image=$uniquename;
     }
     

     $Book->book_name=$request->book_name;
     $Book->author_name=$request->author_name;
    
     if($Book->save()){
 
        $response = [
            'code_status' => '200',
            'message' => 'Book Detail Updated successfully'
        ];
    
         return response($response, 201);

    }else{

        $response = [
            
            'message' => 'Not Updated something wrong'
        ];
    
         

    }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
        $user=Book::find($book->id);
        if($user->delete()){
            
            $response = [
                'code_status' => '200',
                'message' => 'Book Deleted successfully'
            ];
        
             return response($response, 201);
        }else{
          
            $response = [
            
                'message' => 'Book Not Deleted'
            ];
        
             return response($response, 400);

        }
    }
}
