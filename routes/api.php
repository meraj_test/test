<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;

Route::group(['middleware' => 'api'], function($router) {
    
    Route::resource('user', UserController::class);
    Route::resource('book', BookController::class);
    Route::post('/logout', [UserController::class, 'logout']);
    
});
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/login', [UserController::class, 'login']);