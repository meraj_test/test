<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    public function index()
    {
        //
        return response()->json(auth()->user());
    }

     /**
     * login user
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lastname' => 'required|string|min:2|max:100',
            'email' => 'required|string|email|max:100|unique:users',
            'firstname' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'mobile' => 'required|numeric',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create([
                'lastname' => $request->lastname,
                'firstname' => $request->firstname,
                'mobile' => $request->mobile,
                'gender' => $request->gender,
                'age' => $request->age,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $userdatasingle=User::find($id);
        return $userdatasingle;
    }

        /**
     * Logout user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'User successfully logged out.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $rule= array("firstname" => "required",
            "lastname" => "required",
            "mobile" => "required|numeric",
            "age" => "required",
            "gender" =>"required",
            
            );
       
       $validator=Validator::make($request->all(),$rule);
       if($validator->fails())
       {
            return $validator->errors();
       }else{
        $password=Hash::make($request->password);
        $user=User::find($id);

        $user->firstname=$request->firstname;
        $user->lastname=$request->lastname;
        $user->mobile=$request->mobile;
        $user->age=$request->age;
        $user->gender=$request->gender;
        $user->email=$request->email;
        $user->password=$password;
        if($user->save()){

            $response = [
                'code_status' => '200',
                'message' => 'User Updated successfully'
            ];
        
             return response($response, 201);

        }else{

            echo "Ohh not updated successfully";

        }
           
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=User::find($id);
        if($user->delete()){
            
            $response = [
                'code_status' => '200',
                'message' => 'User Deleted successfully'
            ];
        
             return response($response, 201);
        }else{
          
            $response = [
            
                'message' => 'User Not Deleted'
            ];
        
             return response($response, 400);

        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
